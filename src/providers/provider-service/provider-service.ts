//import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Utilisateur } from '../../model/utilisateur';

/*
  Generated class for the ProviderServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProviderServiceProvider {

    private utilisateurListRef = this.db.list<Utilisateur>('utilisateur-list');

    constructor(private db: AngularFireDatabase) { }

    getUtilisateurList() {
        return this.utilisateurListRef;
    }

    addUtilisateur(utilisateur: Utilisateur) {
        return this.utilisateurListRef.push(utilisateur);
    }

    updateUtilisateur(utilisateur: Utilisateur) {
        return this.utilisateurListRef.update(utilisateur.key, utilisateur);
    }

    removeUtilisateur(utilisateur: Utilisateur) {
        return this.utilisateurListRef.remove(utilisateur.key);
    }

/*   constructor(public http: HttpClient) {
     console.log('Hello ProviderServiceProvider Provider');
   }*/

}
