
import { AngularFireDatabase } from 'angularfire2/database';

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AddItemPage} from "../add-item/add-item";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

    gotoAddItem(){
    this.navCtrl.setRoot('AddItemPage');
    }

}
